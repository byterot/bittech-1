#!/bin/bash -e

COMPRESS=false # Compress fully
OUTNAME=""

while getopts ":n:c" opt; do
	case $opt in
		c)
			COMPRESS=true
		;;
		n)
			OUTNAME=$OPTARG
		;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
		;;
	esac
done

if [ "$OUTNAME" = "" ]; then 
	echo "No output name specified."
	exit 1
fi

#--------------
#- build game -
#--------------
mkdir -p build
mkdir -p bin
cd build
cmake -DCMAKE_TOOLCHAIN_FILE="toolchains/dos.cmake" .. 
make
cd ..
if [ $COMPRESS = false ]; then
	./upx -9 build/$OUTNAME || true 
else
	./upx --ultra-brute build/$OUTNAME || true 
fi
cp build/$OUTNAME bin/$OUTNAME.EXE

#---------------
#- build assets -
#----------------
cd src/tools/
g++ -o packer packer.cpp
cd ../..
src/tools/packer bin/$OUTNAME.DAT || true


# First build after clean/clone we need to copy cwsdpmi
if [ ! -f bin/CWSDPMI.EXE ]; then
	cp CWSDPMI.EXE bin/
fi

echo 'total game size:'
du -bh bin/
du -bh bin/$OUTNAME.EXE
du -bh bin/$OUTNAME.DAT
#stat --printf="%n -> %s\n" bin/*
