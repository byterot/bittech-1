#include <cstdio>
#include <sys/nearptr.h>

int main() {
	if(!__djgpp_nearptr_enable()) {
		printf("Could not enable nearptr. :(\n");
		return 1;
	}

	printf("Hello world!\n");

	__djgpp_nearptr_disable();

	printf("Thank you for playing!\n");
	return 0;
}
