#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
//#include "utils.h"
#include <type_traits>

// Switch between different base types and precisions by configuring these
#define FP_FRACTION_BITS 15
#define FP_INTEGER_BITS 16
#define FP_BASE_MIN INT32_MIN
#define FP_BASE_MAX INT32_MAX
typedef int32_t FixedPointBase;
typedef int64_t FixedPointOverflow; // Used for intermediate results in multiplications and division
#define FP_TRIG_TABLE_DIVIDER 32


#ifdef FP_AUDIT
//# define fp_assert(x) if(!(x)) { fail("FP assertion " #x " failed"); }
# define fp_assert(x)
#else
# define fp_assert(x)
#endif


static constexpr FixedPointBase FP_SCALE_FACTOR = 1 << FP_FRACTION_BITS;


struct FixedPoint {
	FixedPointBase value;

	constexpr inline FixedPoint(FixedPointBase integer, FixedPointBase fractional)
		: value(integer * FP_SCALE_FACTOR + fractional) {

			fp_assert((integer > 0) || (integer <= 0 && fractional <= 0));

			fp_assert(integer >= -(1<<FP_INTEGER_BITS) && integer < ((1<<FP_INTEGER_BITS) - 1));
			fp_assert(fractional > -FP_SCALE_FACTOR && fractional < FP_SCALE_FACTOR);

		}

	explicit constexpr inline FixedPoint(FixedPointBase integer)
		: FixedPoint(integer, 0) {}

	static constexpr inline FixedPoint raw(FixedPointBase value) {
		FixedPoint fp(0);
		fp.value = value;
		return fp;
	}

	FixedPoint() = default;

	template<typename T>
		explicit constexpr operator T() const {
			return static_cast<T>(value / FP_SCALE_FACTOR);
		}
};

static_assert(std::is_trivial_v<FixedPoint>);


template<int Exp>
static constexpr FixedPointBase parse_fixedpoint_pow10() {
	if constexpr(Exp == 0) {
		return 1;
	} else {
		return 10 * parse_fixedpoint_pow10<Exp-1>();
	}
}


template<FixedPointOverflow cur, int frac_count>
static constexpr FixedPoint parse_fixedpoint_fraction() {
	return FixedPoint::raw((cur << FP_FRACTION_BITS) / parse_fixedpoint_pow10<frac_count>());
}


template<FixedPointOverflow cur, int frac_count, char T, char... Ts>
static constexpr FixedPoint parse_fixedpoint_fraction() {
	static_assert(T >= '0' && T <= '9');
	return parse_fixedpoint_fraction<cur*10+T-'0', frac_count+1, Ts...>();
}


template<FixedPointOverflow cur>
static constexpr FixedPoint parse_fixedpoint_unit() {
	return FixedPoint(cur);
}


template<FixedPointOverflow cur, char T, char... Ts>
static constexpr FixedPoint parse_fixedpoint_unit() {
	if constexpr(T == '.') {
		return parse_fixedpoint_fraction<cur, 0, Ts...>();
	} else if(T != '\0') {
		static_assert(T >= '0' && T <= '9');
		return parse_fixedpoint_unit<cur*10+T-'0', Ts...>();
	} 
}


template<char... T>
constexpr FixedPoint operator""_f() {
	return parse_fixedpoint_unit<0, T...>();
}

inline constexpr FixedPoint operator+(FixedPoint a, FixedPoint b) {
	fp_assert((FixedPointOverflow)a.value + (FixedPointOverflow)b.value <= FP_BASE_MAX);
	fp_assert((FixedPointOverflow)a.value + (FixedPointOverflow)b.value >= FP_BASE_MIN);
	return FixedPoint::raw(a.value + b.value);
}


inline constexpr FixedPoint operator-(FixedPoint a, FixedPoint b) {
	fp_assert((FixedPointOverflow)a.value - (FixedPointOverflow)b.value <= FP_BASE_MAX);
	fp_assert((FixedPointOverflow)a.value - (FixedPointOverflow)b.value >= FP_BASE_MIN);
	return FixedPoint::raw(a.value - b.value);
}


inline constexpr FixedPoint operator*(FixedPoint a, FixedPoint b) {
	fp_assert( ((FixedPointOverflow)a.value * (FixedPointOverflow)b.value) / FP_SCALE_FACTOR >= FP_BASE_MIN);
	fp_assert( ((FixedPointOverflow)a.value * (FixedPointOverflow)b.value) / FP_SCALE_FACTOR <= FP_BASE_MAX);

	return FixedPoint::raw((FixedPointBase)((((FixedPointOverflow)a.value) * ((FixedPointOverflow)b.value)) / FP_SCALE_FACTOR));
}

inline constexpr FixedPoint operator/(FixedPoint a, FixedPoint b) {
	fp_assert( b.value != 0);
	fp_assert( ((FixedPointOverflow)a.value * FP_SCALE_FACTOR) / b.value >= FP_BASE_MIN);
	fp_assert( ((FixedPointOverflow)a.value * FP_SCALE_FACTOR) / b.value <= FP_BASE_MAX);

	return FixedPoint::raw((FixedPointBase)(((FixedPointOverflow)a.value * FP_SCALE_FACTOR) / b.value));
}


inline constexpr FixedPoint operator%(FixedPoint a, FixedPoint b) {
	return FixedPoint::raw(a.value % b.value);
}


inline constexpr FixedPoint operator-(FixedPoint a) {
	fp_assert(a.value != FP_BASE_MIN);

	return FixedPoint::raw(-a.value);
}


inline constexpr bool operator<(FixedPoint a, FixedPoint b) {
	return a.value < b.value;
}


inline constexpr bool operator>(FixedPoint a, FixedPoint b) {
	return a.value > b.value;
}


inline constexpr bool operator>=(FixedPoint a, FixedPoint b) {
	return a.value >= b.value;
}


inline constexpr bool operator<=(FixedPoint a, FixedPoint b) {
	return a.value <= b.value;
}


inline constexpr bool operator==(FixedPoint a, FixedPoint b) {
	return a.value == b.value;
}


inline constexpr bool operator!=(FixedPoint a, FixedPoint b) {
	return a.value != b.value;
}

// Note the fp_asserts here may be pretty slow. I don't want to 
// spend any time or readability making them fast.
// Just enable them from time to time to see if we get
// overflows or other problems

static inline constexpr FixedPoint frac(FixedPoint fp) {
	return FixedPoint::raw(fp.value & (FP_SCALE_FACTOR-1));
}


static inline constexpr FixedPointBase fp_to_int(FixedPoint a) {
	return a.value / FP_SCALE_FACTOR;
}

static inline constexpr float fp_to_float(FixedPoint a) {
	return (float)a.value / FP_SCALE_FACTOR;
}


static inline constexpr FixedPoint fp_from_float(float a) {
	return FixedPoint::raw((FixedPointBase)(a * FP_SCALE_FACTOR));
}


static inline constexpr FixedPoint floor(FixedPoint a) {
	return FixedPoint(int(a));
}

static inline constexpr FixedPoint abs(FixedPoint a) {
	return a.value > 0 ? a : FixedPoint(-a.value);
}

static inline constexpr FixedPoint ceil(FixedPoint a) {
	return FixedPoint::raw((a.value + (FP_SCALE_FACTOR - 1)) & ~(FP_SCALE_FACTOR - 1));
}


FixedPoint sin(FixedPoint a);
FixedPoint cos(FixedPoint a);
FixedPoint tan(FixedPoint a);
FixedPoint cot(FixedPoint a);
