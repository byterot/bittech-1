#pragma once

#include <vector>
#include <cstdint>
#include <memory>

#include "clip.h"
#include "pos.h"
#include "graphicsdevice.h"
#include "pcximage.h"
#include "contentmanager.h"

class Tilemap {
	private:
		std::vector<uint8_t> tiles;
		Clip *clip;
		uint16_t width, height;
		uint16_t tile_stride, num_tiles;
		std::shared_ptr<PcxImage> tileset;

	public:
		void init(ContentManager &content, uint16_t size);
		void set_clip(Clip *c);
		void buffer(GraphicsDevice &graphics, Pos &camera);
};
