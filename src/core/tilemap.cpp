#include "tilemap.h"

#include <string>
#include "fpa.h"
#include "util.h"
#include "clip.h"

#define TILE_SIZE 32
#define TILE_SIZE_SHIFT 5

void Tilemap::init(ContentManager &content, uint16_t size) {
	tiles.clear();

	for(uint16_t y = 0; y < size; ++y)
		for(uint16_t x = 0; x < size; ++x)
			tiles.push_back(x % 4);
	
	width = size;
	height = size;

	std::string tilesetimage = "tiles.pcx";
	tileset = content.get_image(tilesetimage);
	


	tile_stride = tileset->width / TILE_SIZE;
	num_tiles = tileset->height / TILE_SIZE * tile_stride;
};

void Tilemap::set_clip(Clip *c) {
	clip = c;
};

void Tilemap::buffer(GraphicsDevice &graphics, Pos &camera) {
	if(clip == NULL) return; // Tilemap requires a clip, impractical otherwise
	
	uint8_t tile = 0;

	// camera coordinates
  uint32_t cx,cy;
	camera.get(cx, cy);

	// tile top-left coordinates
	int32_t tlx, tly;

	// tile clip bounds
	int32_t sx,ex,sy,ey;

	// image start coordinates
	uint32_t tx,ty;

	for(uint16_t j = 0; j < height; ++j)
		for(uint16_t i = 0; i < width; ++i) {

			tlx = sx = cx + (i << TILE_SIZE_SHIFT);
			ex = sx + TILE_SIZE;
			tly = sy = cy + (j << TILE_SIZE_SHIFT);
			ey = sy + TILE_SIZE;
			
			// Apply clip
			sx = max(sx, clip->x);
			ex = min(ex, clip->x + clip->width);
			sy = max(sy, clip->y);
			ey = min(ey, clip->y + clip->height);

			// completely clipped, do not draw
			if(ex <= sx || ey <= sy) continue;

			tile = tiles[j * height + i];

			ty = (tile / width) << TILE_SIZE_SHIFT;
			tx = (tile % width) << TILE_SIZE_SHIFT;
			
			for(uint16_t y = sy; y < ey; ++y)
				for(uint16_t x = sx; x < ex; ++x) {
					graphics.backpixel(x,y, tileset->data[((ty + y - tly) * tileset->width) + (tx + x - tlx)]);
				}
		}
};
