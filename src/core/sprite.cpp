#include "sprite.h"

#include <memory>
#include "pcximage.h"
#include "fpa.h"
#include "graphicsdevice.h"
#include "util.h"

void Sprite::set_image(std::shared_ptr<PcxImage> i) {
	image = i;
};

void Sprite::set_clip(std::shared_ptr<Clip> c) {
	clip = c;
};

void Sprite::draw(GraphicsDevice &g) {
	int32_t sx, ex, sy, ey;
	sx = fp_int(pos.x);
	ex = sx + image->width;
	sy = fp_int(pos.y);
	ey = sy + image->height;

	if(clip != NULL) {
		sx = max(sx, clip->x);
		ex = min(ex, clip->x + clip->width);
		sy = max(sy, clip->y);
		ey = min(ey, clip->y + clip->height);
	}

	int32_t tx = sx - fp_int(pos.x), 
					ty = sy - fp_int(pos.y),
					stx = tx;
	for(int32_t y = sy; y < ey; ++y) {
		tx = stx;
		for(int32_t x = sx; x < ex; ++x) {
			g.pixel(x,y, image->data[ty * image->width + tx]);
			++tx;
		}
		++ty;
	}
	undraw(g);
	last_pos.x = pos.x;
	last_pos.y = pos.y;
};

void Sprite::undraw(GraphicsDevice &g) {
	int32_t sx, ex, sy, ey;
	int32_t nsx, nex, nsy, ney;
	sx = fp_int(last_pos.x);
	ex = sx + image->width;
	sy = fp_int(last_pos.y);
	ey = sy + image->height;

	nsx = fp_int(pos.x);
	nex = nsx + image->width;
	nsy = fp_int(pos.y);
	ney = nsy + image->height;

	if(clip != NULL) {
		sx = max(sx, clip->x);
		ex = min(ex, clip->x + clip->width);
		sy = max(sy, clip->y);
		ey = min(ey, clip->y + clip->height);
		nsx = max(nsx, clip->x);
		nex = min(nex, clip->x + clip->width);
		nsy = max(nsy, clip->y);
		ney = min(ney, clip->y + clip->height);
	}
	for(int32_t y = sy; y < ey; ++y) {
		for(int32_t x = sx; x < ex; ++x) {
			if(y >= nsy && y < ney && x >= nsx && x < nex) continue;
			g.undraw(x,y);
			//g.pixel(x,y,0);
		}
	}
};
