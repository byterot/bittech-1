#include "clip.h"

#include <cstdint>

#define MAX_WIDTH 320
#define MAX_HEIGHT 200

static int16_t clamp(int16_t a, int16_t min, int16_t max) {
	if(a < min) a = min;
	if(a > max) a = max;
	return a;
};

Clip::Clip() {
	x = 0;
	y = 0;
	width = MAX_WIDTH;
	height = MAX_HEIGHT;
};

Clip::Clip(int16_t x, int16_t y, int16_t w, int16_t h) {
	this->x = clamp(x, 0, MAX_WIDTH);
	this->y = clamp(y, 0, MAX_HEIGHT);
	width = clamp(w, 0, MAX_WIDTH - x);
	height = clamp(h, 0, MAX_HEIGHT - y);	
};
