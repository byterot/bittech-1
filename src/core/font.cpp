#include "font.h"

#include <vector>
#include <memory>
#include <cctype>

#include "pcximage.h"
#include "graphicsdevice.h"

#define CHAR_WIDTH 5
#define CHAR_HEIGHT 8

Font::Font(std::shared_ptr<PcxImage> i) {
	image = i;

}

Font::~Font() {

}

void Font::draw_char(GraphicsDevice &graphics, char ch, int16_t x, int16_t y) {
	uint16_t tx=0,ty=0;

	if(std::islower(ch)) ty += 8;
	else if(std::isupper(ch)) ty = 0;
	else return;

	tx = (std::toupper(ch) - 'A') * CHAR_WIDTH;
	
	for(int16_t sy = y; sy < y + CHAR_HEIGHT; ++sy) {
		for(int16_t sx = x; sx < x + CHAR_WIDTH; ++sx) {
			graphics.pixel(sx, sy, image->data[(ty * image->width) + tx]);
			++tx;
		}
		tx -= CHAR_WIDTH;
		++ty;
	}
}
void Font::draw_string(GraphicsDevice &graphics, std::string &str, int16_t x, int16_t y) {
	for(uint16_t i = 0; i < str.length(); ++i) {
		draw_char(graphics, str[i], x, y);
		x += CHAR_WIDTH + 1;
	}
}
