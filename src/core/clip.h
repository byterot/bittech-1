#pragma once

#include <cstdint>

struct Clip {
	int16_t x, y, width, height;

	Clip();
	Clip(int16_t x, int16_t y, int16_t w, int16_t h);
};
