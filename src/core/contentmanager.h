#pragma once

#include <vector>
#include <string>
#include <memory>

#include "pcximage.h"
#include "resourcemanager.h"
#include "graphicsdevice.h"

using namespace std;

class ContentManager {
	private:
		vector<std::shared_ptr<PcxImage>> images;
		ResourceManager *resources;
	public:
		ContentManager();
		~ContentManager();
		vector<string> resource_names;
		void init(ResourceManager *r);
		// load a image if not loaded already
		// return pointer to image
		std::shared_ptr<PcxImage> get_image(string name);
		// unload texture at index
		void unload(std::shared_ptr<PcxImage> i);
};
