#pragma once

#include <cstdio>
#include <cstdint>

class GraphicsDevice {
	private:
		uint32_t vgastart = 0xA0000;

	public:
		bool double_buffer_enabled; // default true
		GraphicsDevice();
		~GraphicsDevice();
		void pixel(uint32_t x, uint32_t y, uint8_t color);
		void backpixel(uint32_t x, uint32_t y, uint8_t color);
		void undraw(uint32_t x, uint32_t y);
		void undrawbuffer();
		void palette(uint8_t color, 
								 uint8_t r, uint8_t g, uint8_t b);
		void getcolor(uint8_t color, uint8_t &r, uint8_t &g, uint8_t &b);
		void draw(void);
		void clear(uint8_t color);
};
