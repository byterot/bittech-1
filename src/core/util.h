#pragma once

static int32_t min(int32_t a, int32_t b) {
	return a < b ? a : b;
};
static int32_t max(int32_t a, int32_t b) {
	return a > b ? a : b;
};
