#include <string>
#include <iostream>
#include <stdexcept>
#include <list>
#include "soundblaster.h"
#include <regex>
#include <dos.h>
#include <iomanip>
#include <dpmi.h>
#include <go32.h>
#include "dosmemory.h"
#include "tools.h"
#include "dma.h"

class SoundblasterEnvNotSetup: public std::runtime_error {
public:
  SoundblasterEnvNotSetup()
    : std::runtime_error("The BLASTER environment variable is not set") {}
};

static UniqueFarPtr<int16_t> sampleBuffer;
const size_t sampleBufferSize = 16384 / 2; //;* 2;
static uint16_t baseAddress;
static uint16_t irq;
static uint16_t dmaChannel;

// SB16 port offsets
const uint8_t BLASTER_RESET_PORT                  = 0x6;
const uint8_t BLASTER_READ_PORT                   = 0xA;
const uint8_t BLASTER_WRITE_PORT                  = 0xC;
const uint8_t BLASTER_READ_BUFFER_STATUS_PORT     = 0xE;
const uint8_t BLASTER_INTERRUPT_ACKNOWLEDGE_8BIT  = 0xE;
const uint8_t BLASTER_INTERRUPT_ACKNOWLEDGE_16BIT = 0xF;
const uint8_t BLASTER_MIXER_OUT_PORT              = 0x4;
const uint8_t BLASTER_MIXER_IN_PORT               = 0x5;

const uint8_t BLASTER_MIXER_INTERRUPT_STATUS      = 0x82;
const uint8_t BLASTER_16BIT_INTERRUPT             = 0x02;

const uint8_t BLASTER_READ_BUFFER_STATUS_AVAIL    = 0x80;
const uint8_t BLASTER_WRITE_BUFFER_STATUS_UNAVAIL = 0x80;
const uint8_t BLASTER_READY_BYTE                  = 0xAA;


// SB16 command constants
const uint8_t BLASTER_SET_OUTPUT_SAMPLING_RATE    = 0x41;
const uint8_t BLASTER_PROGRAM_16BIT_IO_CMD        = 0xB0;
const uint8_t BLASTER_PROGRAM_16BIT_FLAG_FIFO     = 0x02;
const uint8_t BLASTER_PROGRAM_16BIT_FLAG_AUTO_INIT= 0x04;
const uint8_t BLASTER_PROGRAM_16BIT_FLAG_INPUT    = 0x08;
const uint8_t BLASTER_PROGRAM_STEREO              = 0x20;
const uint8_t BLASTER_PROGRAM_SIGNED              = 0x10;

const uint8_t BLASTER_SPEAKER_ON_CMD              = 0xD1;


// PIC
const uint8_t PIC1_COMMAND = 0x20;
const uint8_t PIC2_COMMAND = 0xA0;
const uint8_t PIC1_DATA    = 0x21;
const uint8_t PIC2_DATA    = 0xA1;
const uint8_t PIC_EOI      = 0x20;

const uint8_t PIC_IRQ07_MAP = 0x08;
const uint8_t PIC_IRQ8F_MAP = 0x70;


// ISR data
static _go32_dpmi_seginfo oldBlasterHandler, newBlasterHandler;
int writePage = 0;
bool pageWritten = false;
extern uint8_t *soundData;
//int offset = 0;

std::list<int16_t*> sampleQueue;

static int16_t* (*getSamples)();

static void writeDSP(uint8_t value) {
  while((inportb(baseAddress + BLASTER_WRITE_PORT) & 
          BLASTER_WRITE_BUFFER_STATUS_UNAVAIL) != 0)
    ;

  outportb(baseAddress + BLASTER_WRITE_PORT, value);
}


static uint8_t readDSP() {
  while((inportb(baseAddress + BLASTER_READ_BUFFER_STATUS_PORT) & 
          BLASTER_READ_BUFFER_STATUS_AVAIL) == 0)
    ;

  return inportb(baseAddress + BLASTER_READ_PORT);
}


static bool resetBlaster() {
  for(int j = 0; j < 1000; ++j) {
    outportb(baseAddress + BLASTER_RESET_PORT, 1);
    delay(1);
    outportb(baseAddress + BLASTER_RESET_PORT, 0);
    uint8_t status;

    while(((status = inportb(baseAddress + BLASTER_READ_BUFFER_STATUS_PORT)) 
          & BLASTER_READ_BUFFER_STATUS_AVAIL) == 0)
      ;

    if(inportb(baseAddress + BLASTER_READ_PORT) == BLASTER_READY_BYTE)
      return true;
  }
  return false;
}


static void soundblasterISR() {
  outportb(baseAddress + BLASTER_MIXER_OUT_PORT, 
           BLASTER_MIXER_INTERRUPT_STATUS);
  uint8_t status = inportb(baseAddress + BLASTER_MIXER_IN_PORT);

  if(status & BLASTER_16BIT_INTERRUPT) {
    uint8_t* dst =(uint8_t*)(sampleBuffer.get().get())
      + writePage * sampleBufferSize / 2;
      /*
    if(sampleQueue.size()) {
      int16_t* src = sampleQueue.front();
      sampleQueue.pop_front();
      memcpy( dst, src, sampleBufferSize / 2);
    } else {
      memset(dst, 0, sampleBufferSize / 2);
    }
    */

    memcpy(dst, getSamples(), sampleBufferSize / 2);

    pageWritten = false;
    writePage = 1 - writePage;
    inportb(baseAddress + BLASTER_INTERRUPT_ACKNOWLEDGE_16BIT);
  }

  if(irq >= 8)
    outportb(PIC2_COMMAND, PIC_EOI);
  outportb(PIC1_COMMAND, PIC_EOI);
}


static void setBlasterISR() {
  // Map IRQ to interrupt number on the CPU
  uint16_t interruptVector = irq + irq + (irq < 8)
    ? PIC_IRQ07_MAP
    : PIC_IRQ8F_MAP;

  // Wrap and install ISR
  _go32_dpmi_get_protected_mode_interrupt_vector(interruptVector, 
                                                 &oldBlasterHandler);

  newBlasterHandler.pm_offset = reinterpret_cast<int>(soundblasterISR);
  newBlasterHandler.pm_selector = _go32_my_cs();
  //_go32_dpmi_allocate_iret_wrapper(&newBlasterHandler);
  _go32_dpmi_chain_protected_mode_interrupt_vector(interruptVector,
    &newBlasterHandler);

  // PIC setup: unmask SB IRQ
  if(irq < 8) {
    uint8_t irqmask = inportb(PIC1_DATA);
    outportb(PIC1_DATA, irqmask & ~(1<<irq));
  } else {
    uint8_t irqmask = inportb(PIC2_DATA);
    outportb(PIC2_DATA, irqmask & ~(1<<(irq-8)));
  }
}


static void parseBlasterSettings() {
  auto blasterEnvPtr = getenv("BLASTER");
  if(blasterEnvPtr == nullptr)
    throw SoundblasterEnvNotSetup();

  auto blasterEnv = std::string(blasterEnvPtr);
  
  std::regex pattern("^A([0-9]+) +I([0-9]+) +D([0-9]) +(H([0-9]))?.*$");

  std::smatch match;
  std::regex_match(blasterEnv, match, pattern);

  baseAddress = strtoul(match[1].str().c_str(), nullptr, 16);
  irq         = atoi(match[2].str().c_str());
  dmaChannel  = atoi(match[3].str().c_str());

  if(match.size() >= 6) {
    dmaChannel = atoi(match[5].str().c_str());
  }

  std::cout << "Found SoundBlaster at port " << std::hex << baseAddress
            << "h, IRQ#" << irq << ", DMA " << dmaChannel << std::endl;
}


static void allocSampleBuffer() {
  uint32_t bufferPhys;
  do { // Should run at most twice
    sampleBuffer = UniqueFarPtr<int16_t>(
      dosMalloc(sampleBufferSize).getSegment());

    bufferPhys = sampleBuffer.get().getSegment() * 16;
  } while(bufferPhys % 0x10000 > 0x10000 - sampleBufferSize);

  memset(sampleBuffer.get().get(), 0, sampleBufferSize);
}


static void turnSpeakerOn() {
  writeDSP(BLASTER_SPEAKER_ON_CMD);
}


void startDMAOutput() {
  uint32_t offset = ((sampleBuffer.get().getSegment() << 4) 
                      + sampleBuffer.get().getOffset());

  uint32_t samples = sampleBufferSize / sizeof(int16_t);

  dmaSetupTransfer(dmaChannel, DMADirection::ReadFromMemory, true, false,
                   DMATransferMode::Block, offset, sampleBufferSize);

  // SB16 setup
  writeDSP(BLASTER_SET_OUTPUT_SAMPLING_RATE);
  writeDSP(byte<1>(22050));
  writeDSP(byte<0>(22050));
  writeDSP(BLASTER_PROGRAM_16BIT_IO_CMD
            | BLASTER_PROGRAM_16BIT_FLAG_AUTO_INIT
            | BLASTER_PROGRAM_16BIT_FLAG_FIFO);
  writeDSP(BLASTER_PROGRAM_SIGNED);
  writeDSP(byte<0>(samples/2-1));
  writeDSP(byte<1>(samples/2-1));
}


void soundblasterInit(int16_t* (*getsamplesproc)()) {
  getSamples = getsamplesproc;
  parseBlasterSettings();
  resetBlaster();
  allocSampleBuffer();
  setBlasterISR();
  turnSpeakerOn();
  startDMAOutput();
}


size_t soundblasterNeedSamples() {
  return !pageWritten;
}


void soundblasterPassSamples(int16_t* data) {
  disable();
  sampleQueue.push_back(data);
  enable();
}


size_t soundblasterGetSamplesPerBuffer() {
  return sampleBufferSize / (2*sizeof(int16_t));
}

