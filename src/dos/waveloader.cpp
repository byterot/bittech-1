#include <iostream>
#include <fstream>
#include "waveloader.h"
#include "sound.h"
#include "dos.h"

struct WaveHeader {
  uint32_t chunkId;
  uint32_t chunkSize;
  uint32_t format;
  uint32_t subchunk1Id;
  uint32_t subchunk1Size;
  uint16_t audioFormat;
  uint16_t numChannels;
  uint32_t sampleRate;
  uint32_t byteRate;
  uint16_t blockAlign;
  uint16_t bitsPerSample;
  uint32_t subchunk2Id;
  uint32_t subchunk2Size;
};

const static uint32_t RIFF_CHUNK_ID   = 0x46464952;
const static uint32_t WAVE_FORMAT     = 0x45564157;
const static uint32_t FORMAT_CHUNK_ID = 0x20746d66;
const static uint32_t DATA_CHUNK_ID   = 0x61746164;
const static uint32_t FORMAT_CHUNK_SIZE = 16;
const static uint16_t AUDIO_FORMAT_PCM = 1;

static_assert(sizeof(WaveHeader) == 44,
              "WaveHeader padding problems");

class WaveLoadException: public std::runtime_error {
public:
  WaveLoadException(std::string what)
    : std::runtime_error("Wave load error: " + what) {}

};

Sound loadWaveFile(std::string filename) {
  std::ifstream input(filename, std::ios::binary);
  std::cout << input.good() << std::endl;
  
  WaveHeader header;
  if(input.read(reinterpret_cast<char*>(&header), sizeof(header)).gcount() !=
    sizeof(header)) {
    throw WaveLoadException("Short read");
  }

  if(header.chunkId != RIFF_CHUNK_ID
      || header.format != WAVE_FORMAT
      || header.subchunk1Id != FORMAT_CHUNK_ID
      || header.subchunk2Id != DATA_CHUNK_ID) {
    throw WaveLoadException("Invalid header");
  }

  if(header.subchunk1Size != FORMAT_CHUNK_SIZE
      || header.audioFormat != AUDIO_FORMAT_PCM) {
    throw WaveLoadException("Invalid Audio Format (only PCM supported)");
  }
  
  if(header.numChannels != 1) {
    throw WaveLoadException("Invalid Audio Format (only 1 channel supported)");
  }


  if(header.sampleRate != 22050) {
    throw WaveLoadException("Invalid Audio Format (only 22050Hz supported)");
  }

  if(header.bitsPerSample != 16) {
    throw WaveLoadException("Invalid Audio Format (only 16 Bit supported)");
  }

  if(header.byteRate != header.sampleRate * header.numChannels *
    header.bitsPerSample / 8) {
    throw WaveLoadException("Byte rate doesn't match header info");
  }

  if(header.blockAlign != header.numChannels * header.bitsPerSample / 8) {
    throw WaveLoadException("Block alignment doesn't match header info");
  }

  Sound sound(header.subchunk2Size / 2);

  if(input.read(reinterpret_cast<char*>(sound.getSamplePointer(0)),
      header.subchunk2Size).gcount() != static_cast<std::streamsize>(header.subchunk2Size)) {
    throw WaveLoadException("Short read while reading samples");
  }

  return sound;
}
