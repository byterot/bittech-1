#include <iostream>
#include <dos.h>
#include "dma.h"
#include "tools.h"

struct DMARegisters {
  uint8_t startAddressRegister;
  uint8_t countRegister;
  uint8_t singleChannelMaskRegister;
  uint8_t modeRegister;
  uint8_t flipFlopResetRegister;
  uint8_t pageRegister;
};


static const DMARegisters dmaRegisters[] = {
  { 0x00, 0x01, 0x0A, 0x0B, 0x0C, 0x87 },
  { 0x02, 0x03, 0x0A, 0x0B, 0x0C, 0x83 },
  { 0x04, 0x05, 0x0A, 0x0B, 0x0C, 0x81 },
  { 0x06, 0x07, 0x0A, 0x0B, 0x0C, 0x82 },
  { 0xC0, 0xC2, 0xD4, 0xD6, 0xD8, 0x8F },
  { 0xC4, 0xC6, 0xD4, 0xD6, 0xD8, 0x8B },
  { 0xC8, 0xCA, 0xD4, 0xD6, 0xD8, 0x89 },
  { 0xCC, 0xCE, 0xD4, 0xD6, 0xD8, 0x8A }
};


void Outport(uint8_t port, uint8_t val) {
  std::cout << "outportb(" << std::hex << int(port) << ", " << int(val) << ")" <<
    std::endl;
  outportb(port, val);
}


void dmaSetupTransfer(int             channel,
                      DMADirection    direction,
                      bool            autoReload,
                      bool            down,
                      DMATransferMode mode,
                      uint32_t        startAddress,
                      uint32_t        count) {

  uint8_t modeByte = toUnderlying(direction)
                     | toUnderlying(mode)
                     | (int(autoReload) << 4)
                     | (int(down)       << 5)
                     | (channel & 0x03);

  uint8_t maskEnable = (channel & 0x03) | 0x04;
    
  uint32_t offset = startAddress;
  if(channel > 3) {
    std::cout << ">3" << std::endl;
    offset >>= 1;
    count >>= 1;
  }
  std::cout << std::hex << offset << std::endl;

  uint8_t page = byte<2>(startAddress);

  Outport(dmaRegisters[channel].singleChannelMaskRegister, maskEnable);
  Outport(dmaRegisters[channel].flipFlopResetRegister,     0x00);
  Outport(dmaRegisters[channel].modeRegister,              modeByte);
  Outport(dmaRegisters[channel].startAddressRegister,      byte<0>(offset));
  Outport(dmaRegisters[channel].startAddressRegister,      byte<1>(offset));
  Outport(dmaRegisters[channel].countRegister,             byte<0>(count-1));
  Outport(dmaRegisters[channel].countRegister,             byte<1>(count-1));
  Outport(dmaRegisters[channel].pageRegister,              page);
  Outport(dmaRegisters[channel].singleChannelMaskRegister, maskEnable & 0x03);
}
