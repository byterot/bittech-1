#pragma once

#include <memory>
#include <cstdint>
#include <vector>
#include "sound.h"

struct MixedSound {
  int offset;
  std::shared_ptr<Sound> sound;
  MixedSound(int offset, std::shared_ptr<Sound> sound)
    : offset(offset), sound(sound) {}
};

class Mixer {
  std::unique_ptr<int16_t[]> data;
  size_t samplesPerBlock;
  int currentBlock = 0;
  std::vector<MixedSound> sounds;
  bool canmix = false;
public:
  Mixer();
  int16_t* getNextBlock();
  void play(std::shared_ptr<Sound> newsound);
  void mix();
};

