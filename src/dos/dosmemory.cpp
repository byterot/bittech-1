#include "dosmemory.h"

FarPtr<void> dosMalloc(size_t size) {
  int selector;
  int segment = __dpmi_allocate_dos_memory((size+15)/16, &selector);

  if(segment == -1) {
    throw DOSAllocationError();
  }

  dosAllocSegmentToSelector[segment] = selector;

  FarPtr<void> ptr {segment, 0};

  return ptr;
}
