#include "sounddevice.h"

#include <cstdint>
#include <stdio.h>
#include <pc.h>
#include <go32.h>
#include <dpmi.h>

#include "dos.h"
#include "radplayer.h"

#include "mixer.h"
#include "waveloader.h"
#include "sound.h"
#include "soundblaster.h"

#include <map>
#include <string>

#define OPL3_BASE 0x388
#define RTC_INTERRUPT 0x1C

static int duration;
static uint8_t *tune = NULL;
static RADPlayer player;

static bool playing_opl3;

static _go32_dpmi_seginfo old_isr;
static _go32_dpmi_seginfo new_isr;

static std::map<std::string, std::shared_ptr<Sound>> sound_map;

static Mixer mixer;

int16_t* passSamples() {
	return mixer.getNextBlock();
}

static void clock_isr() {
	if(playing_opl3) player.Update();
}

static inline void opl3_write(int32_t base, uint16_t reg, uint8_t val) {
  outportb(static_cast<uint16_t>(base), reg);
  inportb(static_cast<uint16_t>(OPL3_BASE));
  outportb(static_cast<uint16_t>(base+1), val);
  inportb(static_cast<uint16_t>(OPL3_BASE));
  inportb(static_cast<uint16_t>(OPL3_BASE));
  inportb(static_cast<uint16_t>(OPL3_BASE));
}
static void opl3(void* args, uint16_t reg, uint8_t val) {
  if(reg & (0xFF << 8)) {
    opl3_write(OPL3_BASE+2, reg, val);
  } else {
    opl3_write(OPL3_BASE, reg, val);
  }
}

SoundDevice::SoundDevice() {
	playing_opl3 = false;


	disable();
  _go32_dpmi_get_protected_mode_interrupt_vector(RTC_INTERRUPT, &old_isr);
  new_isr.pm_offset = (int)clock_isr;
  new_isr.pm_selector = _go32_my_cs();
  _go32_dpmi_allocate_iret_wrapper(&new_isr); 
  _go32_dpmi_set_protected_mode_interrupt_vector(RTC_INTERRUPT, &new_isr);
	enable();


	soundblasterInit(&passSamples);
}

SoundDevice::~SoundDevice() {
	if(playing_opl3) {
   stop_opl3();
  }
	if(tune != NULL)
		player.Stop();

	// Release interrupt, do some cleanup.
	disable();
	_go32_dpmi_set_protected_mode_interrupt_vector(RTC_INTERRUPT, &old_isr);
	_go32_dpmi_free_iret_wrapper(&new_isr);
	enable();

	sound_map.clear();

	delete tune;
}

void SoundDevice::load_opl3(FILE *fp) {
	uint16_t size = fgetc(fp) << 8;
	size = size | fgetc(fp);

  tune = new uint8_t[size];
  bool copied = (fread(tune, size, 1, fp) > 0);
  if(!copied) {}
	
  player.Init(tune, opl3, 0);

  int hertz = player.GetHertz();

  uint32_t d = 1193180 / hertz;
  outportb(0x43, 0xb6); // signal PIT
  outportb(0x40, (uint8_t)d); // 0x40 is channel 0 data register on PIT
  outportb(0x40, (uint8_t)(d >> 8));
	fclose(fp);
}

void SoundDevice::play_opl3() {
	playing_opl3 = true;
}

void SoundDevice::stop_opl3() {
	playing_opl3 = false;
}

void SoundDevice::load_sound(std::string name, std::string file) {
	sound_map.insert(sound_map.begin(), std::pair<std::string, std::shared_ptr<Sound>>(name, std::make_shared<Sound>(loadWaveFile(file))));
}
void SoundDevice::play_sound(std::string name) {
	mixer.play(sound_map.find(name)->second);
}
void SoundDevice::update() {
	mixer.mix();
}
