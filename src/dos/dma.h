#pragma once

#include <cstdint>

enum class DMADirection: std::uint8_t {
  WriteToMemory  = 0x04,
  ReadFromMemory = 0x08
};


enum class DMATransferMode: std::uint8_t {
  OnDemand = 0x00,
  Single   = 0x40,
  Block    = 0x80,
  Cascade  = 0xC0
};

void dmaSetupTransfer(int             channel,
                      DMADirection    direction,
                      bool            autoReload,
                      bool            down,
                      DMATransferMode mode,
                      uint32_t        startAddress,
                      uint32_t        count);

