// <rnlf> when you find an IRQ list, bitrot, add 8 if the IRQ number is 7 or lower, and 0x70 if it's 8 or higher -> that's the interrupt vector you have to use

#include "inputdevice.h"

#include <cstdint>
#include <string.h> // memset
#include <dpmi.h>
#include <dos.h>
#include <pc.h> // port control
#include <go32.h>
#include <stdio.h>

#define KEYBOARD_INTERRUPT 9
#define KEYBOARD_DATA_PORT 0x60

#define SCANCODE_PREFIX 0xE0    // prefix sent when scancode is 16 bit
#define SCANCODE_OFFSET 0x100   // ??
#define SCANCODE_MAX    0x200   // highest scancode we want to listen for

#define QUEUE_SIZE      64

static uint8_t    input_queue[QUEUE_SIZE]; // Interrupt controlled
static uint32_t   queue_position;
static uint8_t    shadow_queue[QUEUE_SIZE]; // Used by fetch()
static uint8_t    keys_down[SCANCODE_MAX];
static uint8_t    keys_pressed[SCANCODE_MAX];
static uint8_t    keys_released[SCANCODE_MAX];

static _go32_dpmi_seginfo old_isr;
static _go32_dpmi_seginfo new_isr;

static uint8_t mouse_button;
static bool mouse_detected;

// keyboard interrupt service routine
static void isr(void) 
{
	if(queue_position < QUEUE_SIZE)
	{
		input_queue[queue_position] = inp(KEYBOARD_DATA_PORT);
		++queue_position;
	}

	// End of uint32_terrupt
	outportb(0x20, 0x20);   // 0x20 = PIC 1
	// 0x20 = End of Interrupt
}

static void bind(void)
{
	// Hook uint32_to IRQ 1
	_go32_dpmi_get_protected_mode_interrupt_vector(KEYBOARD_INTERRUPT, &old_isr);
	new_isr.pm_offset = (uint32_t)isr;
	new_isr.pm_selector = _go32_my_cs();
	_go32_dpmi_allocate_iret_wrapper(&new_isr); 
	_go32_dpmi_set_protected_mode_interrupt_vector(KEYBOARD_INTERRUPT, &new_isr);

	// Initialize the queue
	queue_position = 0;
	memset(keys_down,       0, sizeof(keys_down));
	memset(keys_pressed,    0, sizeof(keys_pressed));
	memset(keys_released,   0, sizeof(keys_released));

	__dpmi_regs regs;
	regs.x.ax = 0x0;
	__dpmi_int(0x33, &regs); // query mouse connected

	//printf("AX: %d\nBX: %d\n", regs.x.ax, regs.x.bx);
	//delay(2500);

	mouse_detected = !(regs.x.ax == 0 || regs.x.ax == -1);
}

static void unbind(void)
{
	// Release uint32_terrupt, do some cleanup.
	_go32_dpmi_set_protected_mode_interrupt_vector(KEYBOARD_INTERRUPT, &old_isr);
	_go32_dpmi_free_iret_wrapper(&new_isr);
}

void InputDevice::fetch()
{
	// Copy input queue uint32_to shadow queue
	memcpy(shadow_queue, input_queue, queue_position);
	// Reset queue up until queue position
	memset(input_queue, 0, queue_position);
	uint32_t queue_length = queue_position;
	queue_position = 0;

	// If only first byte of two-byte scancode was received,
	// keep the first byte in the input queue
	if(shadow_queue[queue_length-1] == SCANCODE_PREFIX)
	{
		input_queue[0] = SCANCODE_PREFIX;
		queue_position = 1;
		--queue_length;
	}

	memset(keys_pressed,  0, sizeof(keys_pressed));
	memset(keys_released, 0, sizeof(keys_released));

	for(uint32_t i = 0; i < queue_length; ++i)
	{
		uint32_t scancode;
		if(shadow_queue[i] == SCANCODE_PREFIX)
		{
			++i;
			scancode = SCANCODE_PREFIX + (shadow_queue[i] & 0x7F);
		} 
		else
		{
			scancode = shadow_queue[i] & 0x7F;
		}

		bool released = (shadow_queue[i] & 0x80) != 0;

		if(released && keys_down[scancode])
		{
			keys_released[scancode] = true;
		}
		if(!released && !keys_down[scancode])
		{
			keys_pressed[scancode] = true;
		}
		keys_down[scancode] = !released;
	}

	// fetch mouse input
	__dpmi_regs regs;
	regs.x.ax = 0x3;
	regs.x.bx = regs.x.cx = regs.x.dx = 0;
	__dpmi_int(0x33, &regs);
	mouse_button = regs.x.bx;
	mouse_x = regs.x.cx;
	mouse_y = regs.x.dx;
}

InputDevice::InputDevice() {
	bind();
	mouse_enabled = mouse_detected;
	mouse_button = 0;
	mouse_x = 0;
	mouse_y = 0;
}

InputDevice::~InputDevice() {
	unbind();
}

void InputDevice::update(void) {
	fetch();
}

void InputDevice::set_mouse_pos(int16_t x, int16_t y) {
	__dpmi_regs regs;
	regs.x.ax = 0x4;
	regs.x.cx = x;
	regs.x.dx = y;
	__dpmi_int(0x33, &regs);
	mouse_x = x;
	mouse_y = y;
}

bool InputDevice::key_down(uint32_t scancode) {
    return keys_down[scancode];
}

bool InputDevice::key_pressed(uint32_t scancode) {
    return keys_pressed[scancode];
}

bool InputDevice::key_released(uint32_t scancode) {
    return keys_released[scancode];
}

bool InputDevice::mouse_pressed(uint8_t button) {
	return mouse_button == button;
}

