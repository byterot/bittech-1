cmake_minimum_required(VERSION 3.2)

project(bittech1)
#set(CMAKE_CXX_STANDARD 17)
#set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "-std=gnu++17 -O3")

include_directories(src/core src/test src/engine src)

file(GLOB SOURCES "src/core/*.cpp" "src/test/*.cpp" "src/engine/*.cpp" "src/*.cpp")


if(CMAKE_SYSTEM_NAME STREQUAL "Sdl")
	include_directories(src/sdl)
	file(GLOB PLATFORM_SOURCES "src/sdl/*.cpp")
else()
	include_directories(src/dos)
	file(GLOB PLATFORM_SOURCES "src/dos/*.cpp")
endif()


add_executable(bttest ${SOURCES} ${PLATFORM_SOURCES})
