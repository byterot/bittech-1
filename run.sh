#!/bin/bash -e

NAME=bttest
COMPRESS=false # Compress fully

while getopts ":c" opt; do
	case $opt in
		c)
			COMPRESS=true
		;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
		;;
	esac
done

if [ $COMPRESS = false ]; then
	./build.sh -n $NAME
else
	./build.sh -n $NAME -c
fi
cd bin
dosbox -conf ../dosbox.conf -c "mount x $PWD" -c "x:" -c "$NAME.EXE" 
